CREATE DATABASE empleados_db

SHOW DATABASES;

DROP TABLE empleados_db.empleados;

CREATE TABLE empleados_db.empleados(
	email varchar(300),
    primer_apellido char(20) not null,
    segundo_apellido char(20) ,
	primer_nombre char(20) not null,
    otro_nombre char(20) ,
    pais char(100) not null,
    tipo_identificacion char(40) not null,
    numero_identificacion varchar(20) not null,
    fecha datetime not null,
	primary key (email)
    );
    
    SELECT * FROM empleados_db.empleados;
 
	DELETE FROM empleados_db.empleados
     where email = 'juan.perez@cidenet.com.co';
 
    UPDATE empleados_db.empleados
    set otro_nombre = 'Jorge'
    where email = 'juan.perez@cidenet.com.co';
    
    Insert into empleados_db.empleados(email,primer_apellido,segundo_apellido,primer_nombre, 
									  otro_nombre,pais,tipo_identificacion,numero_identificacion,fecha)
	values('juan.perez@cidenet.com.co','Perez','Castro', 'Juan',null,'Colombia','cedula de ciudadania', 
			'12345678', current_time());
    
    
    
    