
package com.mycompany.javaclientempleados;

import java.sql.*;

public class Main {

    public static void main(String[] args) throws SQLException {
        ListarRegistros();
        //InsertarRegistros("marco.espitia@cidnet.com","Espitia", "Suarez", 
          //               "Marco"," ","Colombia","Pasaporte", "1049636949");

        // EditarRegistros("mario.espitia@cidnet.com","Espitia", "Martinez","Andres","Jose","Francia","Pasaporte", "1049636949",2);
        
        
        //EliminarRegistros(4);
        
        ListarRegistros();

    }
    
    static void ListarRegistros() throws SQLException{
             Connection conectar = DriverManager.getConnection(
                                             "jdbc:mysql://localhost/empleados_db?serverTimezone=UTC",
                                             "root",
                                             "123456"
       );
       
       System.out.println("Conexion Exitosa");
        
        String sql =   " SELECT * FROM empleados ";
        PreparedStatement ps = conectar.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        
        while(rs.next()){
           int id = rs.getInt("id_empleado");
           String email_empleado = rs.getString("email");
           String primer_apellido_empleado = rs.getString("primer_apellido");
           String segundo_apellido_empleado = rs.getString("segundo_apellido");
           String primer_nombre_empleado = rs.getString("primer_nombre");
           String otro_nombre_empleado = rs.getString("otro_nombre");
           String pais_empleado = rs.getString("pais");
           String tipo_identificacion_empleado = rs.getString("tipo_identificacion");
           String numero_identificacion = rs.getString("numero_identificacion");
           String fecha_registro = rs.getString("fecha");
           
           System.out.printf("%d %s %s %s %s %s %s %s %s \n",id,email_empleado, primer_apellido_empleado,
           segundo_apellido_empleado, primer_nombre_empleado, otro_nombre_empleado, pais_empleado,
           tipo_identificacion_empleado,numero_identificacion, fecha_registro);
        }
       
        rs.close();
        ps.close();
        conectar.close();
    } 
    
     static void InsertarRegistros(String email, String primer_apellido, String segundo_apellido,
             String primer_nombre, String otro_nombre, String pais, String tipo_identificacion,
             String numero_identificacion
             ) throws SQLException{
             Connection conectar = DriverManager.getConnection(
                                             "jdbc:mysql://localhost/empleados_db?serverTimezone=UTC",
                                             "root",
                                             "123456"
       );
       
       System.out.println("Conexion Exitosa");
        
        String sql =   " Insert into empleados_db.empleados(email,primer_apellido,segundo_apellido, primer_nombre,otro_nombre,pais,tipo_identificacion,numero_identificacion,fecha)values(?,?,?,?,?,?,?,?,current_time())";
        PreparedStatement ps = conectar.prepareStatement(sql);
        ps.setString(1, email);
        ps.setString(2, primer_apellido);
        ps.setString(3, segundo_apellido);
        ps.setString(4, primer_nombre);
        ps.setString(5, otro_nombre); 
        ps.setString(6, pais);
        ps.setString(7, tipo_identificacion);  
        ps.setString(8, numero_identificacion);
        
        ps.executeUpdate();
        ps.close();
        conectar.close();
    } 
     
    
       static void EditarRegistros(String email, String primer_apellido, String segundo_apellido,
             String primer_nombre, String otro_nombre, String pais, String tipo_identificacion,
             String numero_identificacion, int id
             ) throws SQLException{
             Connection conectar = DriverManager.getConnection(
                                             "jdbc:mysql://localhost/empleados_db?serverTimezone=UTC",
                                             "root",
                                             "123456"
       );
       
      
        
        String sql =   " UPDATE empleados SET email=?, primer_apellido=?, segundo_apellido=?, primer_nombre=?, otro_nombre=?, pais=?, tipo_identificacion=?, numero_identificacion=? WHERE id_empleado=?";
        PreparedStatement ps = conectar.prepareStatement(sql);
        ps.setString(1, email);
        ps.setString(2, primer_apellido);
        ps.setString(3, segundo_apellido);
        ps.setString(4, primer_nombre);
        ps.setString(5, otro_nombre); 
        ps.setString(6, pais);
        ps.setString(7, tipo_identificacion);  
        ps.setString(8, numero_identificacion);
        ps.setInt(9,id);
       
        
        ps.executeUpdate();
        ps.close();
        conectar.close();
    } 
       
       static void EliminarRegistros( int id) throws SQLException{
             Connection conectar = DriverManager.getConnection(
                                             "jdbc:mysql://localhost/empleados_db?serverTimezone=UTC",
                                             "root",
                                             "123456"
       );
       
      
        
        String sql =   "DELETE FROM empleados where id_empleado = ?;";
        PreparedStatement ps = conectar.prepareStatement(sql);
        ps.setInt(1,id);
       
        
        ps.executeUpdate();
        ps.close();
        conectar.close();
    } 
}