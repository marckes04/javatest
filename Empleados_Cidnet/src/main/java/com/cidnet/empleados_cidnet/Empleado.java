
package com.cidnet.empleados_cidnet;


public class Empleado {

   private int id;
   private String email;
   private String primer_apellido;
   private String segundo_apellido;
   private String primer_nombre;
   private String otro_nombre;
   private String pais;
   private String tipo_identificacion;
   private String numero_identificacion;
   private String fecha;


    public Empleado(int id) {
        this.id = id;
    }

    public Empleado(String email, String primer_apellido, String segundo_apellido, String primer_nombre, String otro_nombre, String pais, String tipo_identificacion, String numero_identificacion) {
        this.email = email;
        this.primer_apellido = primer_apellido;
        this.segundo_apellido = segundo_apellido;
        this.primer_nombre = primer_nombre;
        this.otro_nombre = otro_nombre;
        this.pais = pais;
        this.tipo_identificacion = tipo_identificacion;
        this.numero_identificacion = numero_identificacion;
    }

    public Empleado(int id, String email, String primer_apellido, String segundo_apellido, String primer_nombre, String otro_nombre, String pais, String tipo_identificacion, String numero_identificacion) {
        this.id = id;
        this.email = email;
        this.primer_apellido = primer_apellido;
        this.segundo_apellido = segundo_apellido;
        this.primer_nombre = primer_nombre;
        this.otro_nombre = otro_nombre;
        this.pais = pais;
        this.tipo_identificacion = tipo_identificacion;
        this.numero_identificacion = numero_identificacion;
    }

    public Empleado(int id, String email, String primer_apellido, String segundo_apellido, String primer_nombre, String otro_nombre, String pais, String tipo_identificacion, String numero_identificacion, String fecha) {
        this.id = id;
        this.email = email;
        this.primer_apellido = primer_apellido;
        this.segundo_apellido = segundo_apellido;
        this.primer_nombre = primer_nombre;
        this.otro_nombre = otro_nombre;
        this.pais = pais;
        this.tipo_identificacion = tipo_identificacion;
        this.numero_identificacion = numero_identificacion;
        this.fecha = fecha;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPrimer_apellido() {
        return primer_apellido;
    }

    public void setPrimer_apellido(String primer_apellido) {
        this.primer_apellido = primer_apellido;
    }

    public String getSegundo_apellido() {
        return segundo_apellido;
    }

    public void setSegundo_apellido(String segundo_apellido) {
        this.segundo_apellido = segundo_apellido;
    }

    public String getPrimer_nombre() {
        return primer_nombre;
    }

    public void setPrimer_nombre(String primer_nombre) {
        this.primer_nombre = primer_nombre;
    }

    public String getOtro_nombre() {
        return otro_nombre;
    }

    public void setOtro_nombre(String otro_nombre) {
        this.otro_nombre = otro_nombre;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getTipo_identificacion() {
        return tipo_identificacion;
    }

    public void setTipo_identificacion(String tipo_identificacion) {
        this.tipo_identificacion = tipo_identificacion;
    }

    public String getNumero_identificacion() {
        return numero_identificacion;
    }

    public void setNumero_identificacion(String numero_identificacion) {
        this.numero_identificacion = numero_identificacion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Empleado{");
        sb.append("id=").append(id);
        sb.append(", email=").append(email);
        sb.append(", primer_apellido=").append(primer_apellido);
        sb.append(", segundo_apellido=").append(segundo_apellido);
        sb.append(", primer_nombre=").append(primer_nombre);
        sb.append(", otro_nombre=").append(otro_nombre);
        sb.append(", pais=").append(pais);
        sb.append(", tipo_identificacion=").append(tipo_identificacion);
        sb.append(", numero_identificacion=").append(numero_identificacion);
        sb.append(", fecha=").append(fecha);
        sb.append('}');
        return sb.toString();
    }
   
   
   
}

