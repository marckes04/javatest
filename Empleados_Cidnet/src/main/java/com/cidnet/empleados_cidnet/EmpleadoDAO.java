/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidnet.empleados_cidnet;

/**
 *
 * @author pc
 */
import static com.cidnet.empleados_cidnet.Conexion.*;
import java.sql.*;
import java.util.*;

public class EmpleadoDAO {

    private Connection conn = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private Empleado empleado;

    public List<Empleado> seleccionar() throws ClassNotFoundException {
        String sql = "SELECT * FROM empleados";
        List<Empleado> empleados = new ArrayList();

        try {
            this.conn = getConexion();
            this.ps = this.conn.prepareStatement(sql);
            this.rs = this.ps.executeQuery();
            while (this.rs.next()) {
                int id = this.rs.getInt("id_empleado");
                String email = this.rs.getString("email");
                String primer_apellido = this.rs.getString("primer_apellido");
                String segundo_apellido = this.rs.getString("segundo_apellido");
                String primer_nombre = this.rs.getString("primer_nombre");
                String otro_nombre = this.rs.getString("otro_nombre");
                String pais = this.rs.getString("pais");
                String tipo_identificacion = this.rs.getString("tipo_identificacion");
                String numero_identificacion = this.rs.getString("numero_identificacion");
                String fecha = this.rs.getString("fecha");

                this.empleado = new Empleado(id, email, primer_apellido, segundo_apellido, primer_nombre,
                        otro_nombre, pais, tipo_identificacion,numero_identificacion, fecha);
                empleados.add(this.empleado);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                Cerrar(this.rs);
                Cerrar(this.ps);
                Cerrar(conn);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return empleados;
    }

    public int inserta(Empleado empleado) throws ClassNotFoundException {
        String sql = "Insert into empleados_db.empleados(email,primer_apellido,segundo_apellido, primer_nombre,otro_nombre,pais,tipo_identificacion,numero_identificacion,fecha)values(?,?,?,?,?,?,?,?,current_time())";
        int registros = 0;

        try {
            this.conn = getConexion();
            this.ps = this.conn.prepareStatement(sql);

            this.ps.setString(1, empleado.getEmail());
            this.ps.setString(2, empleado.getPrimer_apellido());
            this.ps.setString(3, empleado.getSegundo_apellido());
            this.ps.setString(4, empleado.getPrimer_nombre());
            this.ps.setString(5, empleado.getOtro_nombre());
            this.ps.setString(6, empleado.getPais());
            this.ps.setString(7, empleado.getTipo_identificacion());
            this.ps.setString(8, empleado.getNumero_identificacion());
            registros = this.ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                Cerrar(this.ps);
                Cerrar(this.conn);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    public int editar(Empleado empleado) throws ClassNotFoundException {
        String sql = "UPDATE empleados SET email=?, primer_apellido=?, segundo_apellido=?, primer_nombre=?, otro_nombre=?, pais=?, tipo_identificacion=?, numero_identificacion=? WHERE id_empleado=?";
        int registros = 0;

        try {
            this.conn = getConexion();
            this.ps = this.conn.prepareStatement(sql);

             this.ps.setString(1, empleado.getEmail());
            this.ps.setString(2, empleado.getPrimer_apellido());
            this.ps.setString(3, empleado.getSegundo_apellido());
            this.ps.setString(4, empleado.getPrimer_nombre());
            this.ps.setString(5, empleado.getOtro_nombre());
            this.ps.setString(6, empleado.getPais());
            this.ps.setString(7, empleado.getTipo_identificacion());
            this.ps.setString(8, empleado.getNumero_identificacion());
            this.ps.setInt(9, empleado.getId());
            registros = this.ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                Cerrar(this.ps);
                Cerrar(this.conn);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    public int eliminar(Empleado empleado) throws ClassNotFoundException {
        String sql = "DELETE FROM empleados where id_empleado = ?;";
        int registros = 0;

        try {
            this.conn = getConexion();
            this.ps = this.conn.prepareStatement(sql);

            this.ps.setInt(1, empleado.getId());
            registros = this.ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                Cerrar(this.ps);
                Cerrar(this.conn);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
