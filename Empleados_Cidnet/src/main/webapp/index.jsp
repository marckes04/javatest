<%-- 
    Document   : index
    Created on : 31/10/2022, 12:54:34 a. m.
    Author     : LENOVO
--%>
<%@page import="com.cidnet.empleados_cidnet.Empleado"%>
<%@page import="java.util.*"%>
<%@page import="com.cidnet.empleados_cidnet.EmpleadoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
              rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" 
              crossorigin="anonymous">
    </head>
    <body>

        <!-- Modal -->

        <div class="modal-dialog">
            <div class="modal-content">
                <form action="index.jsp" method="POST">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Registrar Empleado</h1>
                    </div>
                    <div class="modal-body">

                        <div class="mb-3">
                            <label>Email: </label>
                            <input type="text" name ="email" class="form-control">

                        </div>
                        <div class="mb-3">
                            <label>Primer Apellido: </label>
                            <input type="text" name="primer_apellido" class="form-control">
                        </div>

                        <div class="mb-3">
                            <label>Segundo Apellido: </label>
                            <input type="text" name="segundo_apellido" class="form-control">
                        </div>

                        <div class="mb-3">
                            <label>Primer Nombre: </label>
                            <input type="text" name="primer_nombre" class="form-control">
                        </div>

                        <div class="mb-3">
                            <label>Otro Nombre: </label>
                            <input type="text" name="otro_nombre" class="form-control">
                        </div>

                        <div class="mb-3">
                            <label for="pais">pais: </label>
                            <select id="pais" name="pais">
                                <optgroup label="pais">
                                    <option value="Colombia">Colombia</option>
                                    <option value="Estados_Unidos">Estados Unidos</option>
                                </optgroup>

                            </select>
                        </div>


                        <div class="mb-3">
                            <label for="pais">Tipo Identificacion: </label>
                            <select id="ID_tipo" name="tipo_identificacion">
                                <optgroup label="Identificacion">

                                    <option value="Cedula_Ciudadania"> Cedula Ciudadania </option>
                                    <option value="Cedula_Extanjeria"> Cedula Extranjeria </option>
                                    <option value="Pasaporte"> Pasaporte </option>
                                    <option value="Permiso_Especial">  Permiso Especial</option>

                                </optgroup>
                            </select>
                        </div>

                        <div class="mb-3">
                            <label>Numero de Identificacion: </label>
                            <input type="text" name="numero_identificacion" class="form-control">
                        </div>



                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="enviar">Enviar</button>
                    </div>

                </form>
            </div>

            <%
                EmpleadoDAO Empleadodao = new EmpleadoDAO();

                if (request.getParameter("enviar") != null) {

                    Empleado empleado = new Empleado(
                            request.getParameter("email"),
                            request.getParameter("primer_apellido"),
                            request.getParameter("segundo_apellido"),
                            request.getParameter("primer_nombre"),
                            request.getParameter("otro_nombre"),
                            request.getParameter("pais"),
                            request.getParameter("tipo_identificacion"),
                            request.getParameter("numero_identificacion")
                    );
                    Empleadodao.inserta(empleado);
                }


            %>

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Empleados Registrados</h1>
                    </div>

                    <%                        //EmpleadoDAO Empleadodao = new EmpleadoDAO();
                        List<Empleado> empleados = Empleadodao.seleccionar();

                        for (Empleado empleado : empleados) {


                    %>

                    <div class="modal-body">

                        <div class="card">
                            <div class="card-body" method = "GET">
                                <h5 class="card-title"><%=empleado.getPrimer_nombre()%> <%= empleado.getOtro_nombre()%>  <%=empleado.getPrimer_apellido()%> <%=empleado.getSegundo_apellido()%> </h5>
                                <h6 class="card-subtitle mb-2 text-muted"><%=empleado.getEmail()%></h6>
                                <p class="card-text"><%=empleado.getPais()%></p>
                                <p class="card-text"><%=empleado.getTipo_identificacion()%></p>
                                <p class="card-text"><%=empleado.getNumero_identificacion()%></p>

                                <p class ="blackquoter-footer"> <cite> <%=empleado.getFecha()%></cite></p>
                                <a href="editar.jsp?id=<%=empleado.getId()%>
                                   &&email=<%=empleado.getEmail()%>
                                   &&primer_apellido=<%=empleado.getPrimer_apellido()%>
                                   &&segundo_apellido=<%=empleado.getSegundo_apellido()%>
                                   &&primer_nombre=<%=empleado.getPrimer_nombre()%>
                                   &&otro_nombre=<%=empleado.getOtro_nombre()%>
                                   &&pais=<%=empleado.getPais()%>
                                   &&tipo_identificacion=<%=empleado.getTipo_identificacion()%>
                                   &&numero_identificacion=<%=empleado.getNumero_identificacion()%>
                                   "
                                   
                                   class="card-link">Editar</a>
                                   <a href="eliminar.jsp?id=<%=empleado.getId()%>" class="card-link">Eliminar</a>
                            </div>
                        </div>

                    </div>
                    <%}%>

                </div>

            </div>


    </body>
</html>
