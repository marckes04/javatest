<%-- 
    Document   : editar
    Created on : 31/10/2022, 8:35:48 p. m.
    Author     : LENOVO
--%>

<%@page import="com.cidnet.empleados_cidnet.Empleado"%>
<%@page import="com.cidnet.empleados_cidnet.EmpleadoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
              rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" 
              crossorigin="anonymous">
    </head>
    <body>
         <div class="modal-dialog">
            <div class="modal-content">
                <form action="editar.jsp" method="PUT">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Editar Empleado</h1>
                    </div>
                    <div class="modal-body">

                        <div class="mb-3">
                            <input type="hidden" name = "id" value ="<%=request.getParameter("id")%>">
                            <label>Email: </label>
                            <input type="text" name ="email" class="form-control" value = "<%=request.getParameter("email")%>">
                           
                        </div>
                        <div class="mb-3">
                            <label>Primer Apellido: </label>
                            <input type="text" name="primer_apellido" class="form-control"  value = "<%=request.getParameter("primer_apellido")%>">
                            
                        </div>

                        <div class="mb-3">
                            <label>Segundo Apellido: </label>
                            <input type="text" name="segundo_apellido" class="form-control" value ="<%=request.getParameter("segundo_apellido")%>">
                            
                        </div>

                        <div class="mb-3">
                            <label>Primer Nombre: </label>
                            <input type="text" name="primer_nombre" class="form-control" value=" <%=request.getParameter("primer_nombre")%>">
                           
                        </div>

                        <div class="mb-3">
                            <label>Otro Nombre: </label>
                            <input type="text" name="otro_nombre" class="form-control" value="<%=request.getParameter("otro_nombre")%>">
                            
                        </div>

                        <div class="mb-3">
                            <label for="pais">pais: </label>
                            <select id="pais" name="pais"  value = " <%=request.getParameter("pais")%>"%>">
                                <optgroup label="pais">
                                    <option value="Colombia">Colombia</option>
                                    <option value="Estados_Unidos">Estados Unidos</option>
                                </optgroup>

                            </select>
                            
                        </div>


                        <div class="mb-3">
                            <label for="pais">Tipo Identificacion: </label>
                            <select id="ID_tipo" name="tipo_identificacion" value="<%=request.getParameter("tipo_identificacion")%>" >
                                <optgroup label="Identificacion">

                                    <option value="Cedula_Ciudadania"> Cedula Ciudadania </option>
                                    <option value="Cedula_Extanjeria"> Cedula Extranjeria </option>
                                    <option value="Pasaporte"> Pasaporte </option>
                                    <option value="Permiso_Especial">  Permiso Especial</option>

                                </optgroup>
                            </select>
                            
                            
                        </div>

                        <div class="mb-3">
                            <label>Numero de Identificacion: </label>
                            <input type="text" name="numero_identificacion" class="form-control" value ="<%=request.getParameter("numero_identificacion")%>">
                            
                        </div>



                    </div>
                    <div class="modal-footer">
                         <a href="index.jsp" type="submit" class="btn btn-danger" >Regresar</a>
                         <button type="submit" class="btn btn-primary" name="enviar">Guardar Cambios</button>
                    </div>
            </div>
                            
                            <%
                EmpleadoDAO Empleadodao = new EmpleadoDAO();

                String id = request.getParameter("id");
                
                if (request.getParameter("enviar") != null) {

                    Empleado empleado = new Empleado(
                            Integer.parseInt(id.trim()),
                            request.getParameter("email"),
                            request.getParameter("primer_apellido"),
                            request.getParameter("segundo_apellido"),
                            request.getParameter("primer_nombre"),
                            request.getParameter("otro_nombre"),
                            request.getParameter("pais"),
                            request.getParameter("tipo_identificacion"),
                            request.getParameter("numero_identificacion")
                    );
                    Empleadodao.editar(empleado);
                }


            %>
                            
                            
    </body>
</html>
