<%@page import="com.cidnet.empleados_cidnet.EmpleadoDAO"%>
<%@page import="com.cidnet.empleados_cidnet.Empleado"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
           String id = request.getParameter("id");
           Empleado empleado1= new Empleado(Integer.valueOf(id));
           EmpleadoDAO empleadodao = new EmpleadoDAO();
           empleadodao.eliminar(empleado1);
           request.getRequestDispatcher("index.jsp").forward(request, response);
        
        %>
    </body>
</html>
